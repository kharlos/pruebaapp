package com.example.carlos.androidtestrappi;

import android.app.Application;

import com.example.carlos.androidtestrappi.model.Children;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class App extends Application{
    private static App instance;
    private static String WebServiceURL;
    private static Realm realm;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        WebServiceURL = "https://www.reddit.com/reddits.json";

        Realm.init(instance);
        RealmConfiguration realmConfig = new RealmConfiguration.Builder().deleteRealmIfMigrationNeeded().build();
        Realm.setDefaultConfiguration(realmConfig);
        realm = Realm.getDefaultInstance();

    }

    public static App getInstance() {
        return instance;
    }

    public static String getWebServiceURL() {
        return WebServiceURL;
    }

    public static Realm getRealm() {
        return realm;
    }
}
