package com.example.carlos.androidtestrappi.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.example.carlos.androidtestrappi.App;
import com.example.carlos.androidtestrappi.R;
import com.example.carlos.androidtestrappi.adapter.viewholder.ChildrenViewHolder;
import com.example.carlos.androidtestrappi.model.Children;

import io.realm.RealmResults;

public class ChildrenAdapter extends RecyclerView.Adapter<ChildrenViewHolder> {

    public final RealmResults<Children> childrens;
    private Context context;

    public ChildrenAdapter(Context context) {
        this.context = context;
        childrens = App.getRealm().where(Children.class).findAllSorted("created");
    }

    @NonNull
    @Override
    public ChildrenViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        final View view = LayoutInflater.from(context).inflate(R.layout.item_children_layout, parent, false);
        return new ChildrenViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ChildrenViewHolder holder, int position) {
        holder.getDisplay_name().setText(childrens.get(position).getHeader_title());
        holder.getDescription().setText(Html.fromHtml(childrens.get(position).getDescription_html()));
        Glide
                .with(context)
                .load(childrens.get(position).getIcon_img())
                .into(holder.getIcon_img());
    }

    @Override
    public int getItemCount() {
        return childrens.size();
    }
}
