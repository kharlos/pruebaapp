package com.example.carlos.androidtestrappi.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.carlos.androidtestrappi.R;

public class ChildrenViewHolder extends RecyclerView.ViewHolder {

    private ImageView icon_img;
    private TextView display_name, description;

    public ChildrenViewHolder(View itemView) {
        super(itemView);
        this.icon_img = itemView.findViewById(R.id.icon_img);
        this.display_name = itemView.findViewById(R.id.display_name);
        this.description = itemView.findViewById(R.id.description);
    }

    public ImageView getIcon_img() {
        return icon_img;
    }

    public TextView getDisplay_name() {
        return display_name;
    }

    public TextView getDescription() {
        return description;
    }
}
