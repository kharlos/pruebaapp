package com.example.carlos.androidtestrappi.childrendetail;

import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.carlos.androidtestrappi.App;
import com.example.carlos.androidtestrappi.R;
import com.example.carlos.androidtestrappi.model.Children;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.header_img) ImageView headerImg;
    @BindView(R.id.header_title) TextView headerTitle;
    @BindView(R.id.title) TextView title;
    @BindView(R.id.description) TextView description;

    private String childrenId;
    private Children children;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        if(getIntent().getExtras() != null){
            childrenId = getIntent().getExtras().getString("childrenId");
            children = App.getRealm().where(Children.class).equalTo("id",childrenId).findFirst();
            setContent();
        }

    }



    private void setContent() {
        Glide
                .with(DetailActivity.this)
                .load(children.getBanner_img())
                .into(headerImg);
        headerTitle.setText(children.getHeader_title());
        title.setText(children.getTitle());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            description.setText(Html.fromHtml(children.getPublic_description_html(), Html.FROM_HTML_MODE_COMPACT));
        }else{
            description.setText(Html.fromHtml(children.getDescription_html()));

        }
    }

}
