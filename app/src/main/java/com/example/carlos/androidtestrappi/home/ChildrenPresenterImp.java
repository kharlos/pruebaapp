package com.example.carlos.androidtestrappi.home;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.carlos.androidtestrappi.App;
import com.example.carlos.androidtestrappi.model.Children;
import com.example.carlos.androidtestrappi.presenter.RequestListener;
import com.example.carlos.androidtestrappi.presenter.RequestPresenter;

import org.json.JSONArray;
import org.json.JSONObject;

import io.realm.Realm;

public class ChildrenPresenterImp implements RequestPresenter {

    private RequestListener requestListener;
    private Context context;

    public ChildrenPresenterImp(RequestListener requestListener, Context context) {
        this.requestListener = requestListener;
        this.context = context;
    }

    @Override
    public void getContent() {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                "https://www.reddit.com/reddits.json", null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(final JSONObject response) {
                        App.getRealm().executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                JSONArray childresJson = response.optJSONObject("data").optJSONArray("children");
                                for(int i = 0; i < childresJson.length(); i++){
                                    App.getRealm().copyToRealmOrUpdate(Children.getInstance(childresJson.optJSONObject(i).optJSONObject("data")));
                                }
                                requestListener.onSuccess(false);
                            }
                        });
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ErorResponse", error.toString());
                requestListener.onError(false);
            }
        });
        Volley.newRequestQueue(context).add(request);
    }
}
