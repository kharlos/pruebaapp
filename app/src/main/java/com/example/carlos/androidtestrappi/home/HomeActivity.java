package com.example.carlos.androidtestrappi.home;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.example.carlos.androidtestrappi.R;
import com.example.carlos.androidtestrappi.adapter.ChildrenAdapter;
import com.example.carlos.androidtestrappi.childrendetail.DetailActivity;
import com.example.carlos.androidtestrappi.com.AppHelper;
import com.example.carlos.androidtestrappi.com.RecyclerItemClickListener;
import com.example.carlos.androidtestrappi.presenter.RequestListener;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity implements RequestListener {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.childrenList) RecyclerView childrenList;

    private ChildrenAdapter childrenAdapter;
    private ChildrenPresenterImp mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        childrenList.setLayoutManager(new LinearLayoutManager(HomeActivity.this));

        mPresenter = new ChildrenPresenterImp(this, this);
        if(AppHelper.isNetworkAvailable(this)){
            mPresenter.getContent();
        }else{
            onSuccess(true);
        }

    }

    private void setList(){
        childrenAdapter = new ChildrenAdapter(this);
        childrenList.setAdapter(childrenAdapter);
        childrenList.addOnItemTouchListener(
                new RecyclerItemClickListener(this, childrenList,
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override public void onItemClick(View view, int position) {
                                Intent intent = new Intent(HomeActivity.this, DetailActivity.class);
                                intent.putExtra("childrenId", childrenAdapter.childrens.get(position).getId());
                                startActivity(intent);
                            }

                            @Override public void onLongItemClick(View view, int position) {
                            }
                        })
        );
    }

    @Override
    public void onSuccess(Object object) {
        setList();
    }

    @Override
    public void onError(Object object) {

    }

    @Override
    public void onStatusError(int status) {

    }
}
