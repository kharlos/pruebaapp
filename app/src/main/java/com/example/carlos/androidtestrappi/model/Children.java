package com.example.carlos.androidtestrappi.model;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.carlos.androidtestrappi.App;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Children extends RealmObject {
    @PrimaryKey
    private String id;
    private String name;
    private boolean accounts_active_is_fuzzed;
    private String advertiser_category;
    private boolean allow_discovery;
    private boolean allow_images;
    private boolean allow_videogifs;
    private boolean allow_videos;
    private boolean all_original_content;
    private String audience_target;
    private String banner_img;
    private boolean collapse_deleted_comments;
    private int comment_score_hide_mins;
    private long created;
    private long created_utc;
    private String description;
    private String description_html;
    private String display_name;
    private String display_name_prefixed;
    private String header_img;
    private String header_title;
    private boolean hide_ads;
    private String icon_img;
    private String key_color;
    private String lang;
    private boolean link_flair_enabled;
    private boolean original_content_tag_enabled;
    private boolean over18;
    private String public_description;
    private String public_description_html;
    private boolean public_traffic;
    private boolean quarantine;
    private boolean show_media;
    private boolean show_media_preview;
    private boolean spoilers_enabled;
    private String submission_type;
    private String submit_text;
    private String submit_text_html;
    private String submit_text_label;
    private String subreddit_type;
    private long subscribers;
    private String title;
    private String url;
    private boolean wiki_enabled;

    public static Children getInstance(JSONObject jsonObject) {
        Gson gson = new Gson();
        return gson.fromJson(jsonObject.toString(), Children.class);
    }

    public static void getChildrensFromAPI(Context context){
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
                "https://www.reddit.com/reddits.json", null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(final JSONObject response) {
                        App.getRealm().executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                JSONArray childresJson = response.optJSONArray("children");
                                for(int i = 0; i < childresJson.length(); i++){
                                    App.getRealm().copyToRealmOrUpdate(Children.getInstance(childresJson.optJSONObject(i).optJSONObject("data")));
                                }
                            }
                        });
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("ErorResponse", error.toString());
            }
        });
        Volley.newRequestQueue(context).add(request);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isAccounts_active_is_fuzzed() {
        return accounts_active_is_fuzzed;
    }

    public void setAccounts_active_is_fuzzed(boolean accounts_active_is_fuzzed) {
        this.accounts_active_is_fuzzed = accounts_active_is_fuzzed;
    }

    public String getAdvertiser_category() {
        return advertiser_category;
    }

    public void setAdvertiser_category(String advertiser_category) {
        this.advertiser_category = advertiser_category;
    }

    public boolean isAllow_discovery() {
        return allow_discovery;
    }

    public void setAllow_discovery(boolean allow_discovery) {
        this.allow_discovery = allow_discovery;
    }

    public boolean isAllow_images() {
        return allow_images;
    }

    public void setAllow_images(boolean allow_images) {
        this.allow_images = allow_images;
    }

    public boolean isAllow_videogifs() {
        return allow_videogifs;
    }

    public void setAllow_videogifs(boolean allow_videogifs) {
        this.allow_videogifs = allow_videogifs;
    }

    public boolean isAllow_videos() {
        return allow_videos;
    }

    public void setAllow_videos(boolean allow_videos) {
        this.allow_videos = allow_videos;
    }

    public boolean isAll_original_content() {
        return all_original_content;
    }

    public void setAll_original_content(boolean all_original_content) {
        this.all_original_content = all_original_content;
    }

    public String getAudience_target() {
        return audience_target;
    }

    public void setAudience_target(String audience_target) {
        this.audience_target = audience_target;
    }

    public String getBanner_img() {
        return banner_img;
    }

    public void setBanner_img(String banner_img) {
        this.banner_img = banner_img;
    }

    public boolean isCollapse_deleted_comments() {
        return collapse_deleted_comments;
    }

    public void setCollapse_deleted_comments(boolean collapse_deleted_comments) {
        this.collapse_deleted_comments = collapse_deleted_comments;
    }

    public int getComment_score_hide_mins() {
        return comment_score_hide_mins;
    }

    public void setComment_score_hide_mins(int comment_score_hide_mins) {
        this.comment_score_hide_mins = comment_score_hide_mins;
    }

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public long getCreated_utc() {
        return created_utc;
    }

    public void setCreated_utc(long created_utc) {
        this.created_utc = created_utc;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription_html() {
        return description_html;
    }

    public void setDescription_html(String description_html) {
        this.description_html = description_html;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }

    public String getDisplay_name_prefixed() {
        return display_name_prefixed;
    }

    public void setDisplay_name_prefixed(String display_name_prefixed) {
        this.display_name_prefixed = display_name_prefixed;
    }

    public String getHeader_img() {
        return header_img;
    }

    public void setHeader_img(String header_img) {
        this.header_img = header_img;
    }

    public String getHeader_title() {
        return header_title;
    }

    public void setHeader_title(String header_title) {
        this.header_title = header_title;
    }

    public boolean isHide_ads() {
        return hide_ads;
    }

    public void setHide_ads(boolean hide_ads) {
        this.hide_ads = hide_ads;
    }

    public String getIcon_img() {
        return icon_img;
    }

    public void setIcon_img(String icon_img) {
        this.icon_img = icon_img;
    }

    public String getKey_color() {
        return key_color;
    }

    public void setKey_color(String key_color) {
        this.key_color = key_color;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public boolean isLink_flair_enabled() {
        return link_flair_enabled;
    }

    public void setLink_flair_enabled(boolean link_flair_enabled) {
        this.link_flair_enabled = link_flair_enabled;
    }

    public boolean isOriginal_content_tag_enabled() {
        return original_content_tag_enabled;
    }

    public void setOriginal_content_tag_enabled(boolean original_content_tag_enabled) {
        this.original_content_tag_enabled = original_content_tag_enabled;
    }

    public boolean isOver18() {
        return over18;
    }

    public void setOver18(boolean over18) {
        this.over18 = over18;
    }

    public String getPublic_description() {
        return public_description;
    }

    public void setPublic_description(String public_description) {
        this.public_description = public_description;
    }

    public String getPublic_description_html() {
        return public_description_html;
    }

    public void setPublic_description_html(String public_description_html) {
        this.public_description_html = public_description_html;
    }

    public boolean isPublic_traffic() {
        return public_traffic;
    }

    public void setPublic_traffic(boolean public_traffic) {
        this.public_traffic = public_traffic;
    }

    public boolean isQuarantine() {
        return quarantine;
    }

    public void setQuarantine(boolean quarantine) {
        this.quarantine = quarantine;
    }

    public boolean isShow_media() {
        return show_media;
    }

    public void setShow_media(boolean show_media) {
        this.show_media = show_media;
    }

    public boolean isShow_media_preview() {
        return show_media_preview;
    }

    public void setShow_media_preview(boolean show_media_preview) {
        this.show_media_preview = show_media_preview;
    }

    public boolean isSpoilers_enabled() {
        return spoilers_enabled;
    }

    public void setSpoilers_enabled(boolean spoilers_enabled) {
        this.spoilers_enabled = spoilers_enabled;
    }

    public String getSubmission_type() {
        return submission_type;
    }

    public void setSubmission_type(String submission_type) {
        this.submission_type = submission_type;
    }

    public String getSubmit_text() {
        return submit_text;
    }

    public void setSubmit_text(String submit_text) {
        this.submit_text = submit_text;
    }

    public String getSubmit_text_html() {
        return submit_text_html;
    }

    public void setSubmit_text_html(String submit_text_html) {
        this.submit_text_html = submit_text_html;
    }

    public String getSubmit_text_label() {
        return submit_text_label;
    }

    public void setSubmit_text_label(String submit_text_label) {
        this.submit_text_label = submit_text_label;
    }

    public String getSubreddit_type() {
        return subreddit_type;
    }

    public void setSubreddit_type(String subreddit_type) {
        this.subreddit_type = subreddit_type;
    }

    public long getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(long subscribers) {
        this.subscribers = subscribers;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isWiki_enabled() {
        return wiki_enabled;
    }

    public void setWiki_enabled(boolean wiki_enabled) {
        this.wiki_enabled = wiki_enabled;
    }
}
