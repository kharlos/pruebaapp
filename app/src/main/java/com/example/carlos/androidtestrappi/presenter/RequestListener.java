package com.example.carlos.androidtestrappi.presenter;

public interface RequestListener <S,E> {

    void onSuccess(S object);
    void onError(E object);
    void onStatusError(int status);

}

