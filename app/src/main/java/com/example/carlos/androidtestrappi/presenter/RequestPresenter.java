package com.example.carlos.androidtestrappi.presenter;

public interface RequestPresenter {
    void getContent();
}
